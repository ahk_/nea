
# NEA Offline laodsedding Code generator
###An offline loadshedding schedule algorithm which will converts the loadshedding schedule into a small "code"

#### >Note: This algorithm only reduces the number of characters used to represent the schedule of the schedule and not the actual size


###What is NEA ?  
NEA stands for NEAs an Enormous Ass.  
###Why?  
Because they cant care enough to publish the loadshedding schedule as an easily parsable document!  
I mean, [have you seen the Android App on the NEA website?](http://www.nea.org.np/loadshedding.html), its like they made a noob write it in 5 minutes.  
Also, the 3rd party apps on the play store dont update their schedule often enough.  
So to find a way around all this, I started this project so that everyone can get the newest loadshedding schedule offline.

What this project will achieve :
* Fetch the schedule directly from the [NEA website](http://www.nea.org.np/loadshedding.html) or from some other website which always updates their schedules.
* Code an algorithm to  losslessly compress the _display_ size of the schedule down to a maximum of 10 characters.
* Write a decompresser to get the loadshedding schedule form the <=10 characters.



Features of the algorithm:  
* The size of the code must be small (maximum of 7 characters).
* The code can encompass of ASCII, Hex, Decimal or Binary characters.
* The code must only have visible characters which are easily type-able from a cellphone.
* The code should be the only dependency needed to figure out the schedule.

###TODO List
* Currently, the code for one group can be represented with a maximum of 16 characters, We need to cut it down to 4-5 characters for the whole schedule,  
so reduce `16 * 7 = 112 characters into upto 10 characters.
* Write code to fetch the raw schedule from somewhere
* Improve comments in the code and add a better structure    (&#65295;&#20154;&#9685; __ &#9685;&#20154;&#65340; I have no idea about this one)
* Make this README file better. I wrote this while coming up with the techniques, so its not very structured

[More about this project on the official website]( http://ayushjha6.github.io/NEA )
