#!/usr/bin/python2

'''
This is an algorithm which takes a sample loadshedding schedule
and converts it into a small code which can then be used in
apps to get the newest loadshedding schedule.
'''

'''
Step 1:

Convert the raw intervals into a single character-set.
Example:
interval : 8 - 13
we represent it in the form [M]x[d]y[d]
where [M] is the Minus sign, [d] is `.5'
x is `12 - 8'
y is `31 - 8'
generally :for interval a - b, x = 12 - a, y = b - a
'''

MAXIMUM_HOURS_OF_POWERCUT = 12  # The maximum hours of loadshedding/powercut
# at once
# this is NOT the total hours of powercuts in a day

MINIIMUM_TIME_FOR_LOADSHEDDING = 2.0  # Hours
# Minimum hours for which the script should make the algorithm work
# Larger number means faster algorithm, but also less inclusion

MINIMUM_GAP_BETWEEN_POWER_CUTS = 3.0  # Minimum hours of gap between
# two power cuts

BASE_ALPHABET = '''ABCDEFGHIJKLMNOPQRSTUVWXYZ
abcdefghijklmnopqrstuvwxyz
1234567890
-_+=.,@/
'''.replace('\n', '')
# base alphabet for the encode/decode-er


# Make some values positive
MINIIMUM_TIME_FOR_LOADSHEDDING = abs(MINIIMUM_TIME_FOR_LOADSHEDDING)
MAXIMUM_HOURS_OF_POWERCUT = abs(MAXIMUM_HOURS_OF_POWERCUT)
MINIMUM_GAP_BETWEEN_POWER_CUTS = abs(MINIMUM_GAP_BETWEEN_POWER_CUTS)


def convert_interval_to_step1(a, b):
    '''
    converts the raw intervals into a single character-set
    of maximum 7 characters
    '''
    denoters = {
        '-': 'M',
        'x.5': 'Dx',
        'y.5': 'Dy'
    }

    extra_val_x = ""
    extra_val_y = ""

    x = 12 - a
    y = b - a

    x = str(x)
    y = str(y)

    if x.replace("-", "") != x:
        extra_val_x += denoters["-"]

    if x.replace(".5", "") != x:
        extra_val_x += denoters["x.5"]

    if y.replace(".5", "") != y:
        extra_val_y += denoters["y.5"]

    x = x.replace("-", "").replace(".5", "")
    y = y.replace("-", "").replace(".5", "")
    # Since b > a is always true, no need to check for "-" in b -a i.e. y
    # return x_y <extra_val_x> <extra_val_y>
    return "{0} {1} {2}".format(x + "_" + y, extra_val_x, extra_val_y)

'''
The above code changes the interval exclusively into one of these formats

MxDyD
MxDy
MxyD
xDyD
xDy
xyD

Where, M is "-"; x & y are numbers; D is ".5";
We can reduce the size of the above pattern by denoting the variant of M-D-D
with a single unique character.

Let the first D be D1 and the second D be D2,
so,
%empty% - ""
MD1D2 - "L"
MD1 - "M"
MD2 - "N"
D1 - "O"
D2 - "P"
D1D2 - "Q"
'''


def convert_step1_to_step2(step1_code):
    '''
    This function further shortens the code returned from step 1
    by substituting the patterns from step 1 with a single character
   '''
    data = step1_code.split(" ")
    extra_data = "".join(data[1:])

    denoters = {
        "MDxDy": "L",
        "MDx": "M",
        "MDy": "N",
        "Dx": "O",
        "Dy": "P",
        "DxDy": "Q",
        "": ""
    }
    # check which value to denote which denoter
    return "{0} {1}".format(data[0], denoters[extra_data])
'''

Up till now, we have been using only "human-capable" data-sheets
represent out characters, but now, as wee can see, the human
representable sheets fall short, and thus,
we need to use computer-generated data-sets instead.

To represent three Characters, xyL ;
where 0 <= x <= 12 , 1 <= y <= 8 , 1 <= L <= 7 ;
we need to represent 13 * 8 * 7 = 728 different patterns with 1 character each.
The problem is, the normal data-set characters used cannot be used
to represent these characters.

A method is to represent all the patterns by a number.
Here, 546 numbers can represent the whole pattern.
The patterns are below as tuples (x , y , L).

A question could be why we aren't simply using the values we have,
instead of using their represented value from another algorithm.
Well, we want a unique identity for each value, so lets take this
example for if we use the value we have right now.

1122 could be either x=1 , y=12 , L = 2 ; or x = 11 , y = 2 , L = 2.
Thus, to get a unique identifier,
we use the position instead of the actual value.

Values of Y for any Xn(where 0 <= Xn <= 12) = {12 >= E > (12 - Xn)}

The output is based on teh datasheet below.

First off, lets write a X-Posible_Y table.
This table will list the possible values foar a value of X

X		POSSIBLE_Y
0		Null
1		12
2		11 , 12
3		10 , 11 , 12
4		9 , 10 , 11 , 12
5		8 , 9 , 10 , 11 , 12
6		7 , 8 , 9 , 10 , 11 , 12
7		6 , 7 , 8 , 9 , 10 , 11 , 12
8		5 , 6 , 7 , 8 , 9 , 10 , 11 , 12
9		4 , 5 , 6 , 7 , 8 , 9 , 10 , 11 , 12
10		3 , 4 , 5 , 6 , 7 , 8 , 9 , 10 , 11 , 12
11		2 , 3 , 4 , 5 , 6 , 7 , 8 , 9 , 10 , 11 , 12
12		1 , 2 , 3 , 4 , 5 , 6 , 7 , 8 , 9 , 10 , 11 , 12

When the value of X in 0,  there aren't any values of Y.
So, there aren't any values of L as well :)

To do this though, we need to convert the code back to the interval.
'''

'''
FUCK THIS!!! I'm GONNA TAKE A NAP!
'''


def get_interval(x, y, l):
    '''
    returns a, b if possible, else return None
    '''
    l_conf = {
            0: ("{}", "{}"),
            1: ("-{}.5", "{}.5"),
            2: ("-{}.5", "{}"),
            3: ("-{}", "{}.5"),
            4: ("{}.5 ", "{}"),
            5: ("{}", "{}.5"),
            6: ("{}.5", "{}.5"),
            }
    data = l_conf[l]

    x = float(data[0].format(x))
    y = float(data[1].format(y))
    # return a, b where a = 12 - x & b = y + a;
    a, b = 12 - x, 12 + y - x

    # Ignore -ve values and values greater than 24. Also, check if number is
    # less then MINIIMUM_TIME_FOR_LOADSHEDDING
    if (MINIIMUM_TIME_FOR_LOADSHEDDING <= b - a <= MAXIMUM_HOURS_OF_POWERCUT) \
            and b >= 0 and b <= 24 and \
            a >= 0 and a <= 24:
        return 12 - x, 12 + y - x
    else:
        return None


def single_interval_generator():
    for x in range(0, 12 + 1):
        for y in range(0, MAXIMUM_HOURS_OF_POWERCUT + 1):
            for l in range(0, 7):
                val = get_interval(x, y, l)
                if val is not None:
                    yield x, y, l


def get_element_single_interval(pos):
    gen = single_interval_generator()
    for x in range(0, pos - 1):
        gen.next()
    return gen.next()


def days_intervals():
    '''
    the pairs that work will, for two intervals a-b and c-d,
    always have the value of c - b >= GAP_BETWEEN_POWER_CUTS
    '''
    interval1 = single_interval_generator
    interval2 = single_interval_generator

    for in1 in interval1():
        for in2 in interval2():
            i1 = get_interval(in1[0], in1[1], in1[2])
            i2 = get_interval(in2[0], in2[1], in2[2])
            if i1 is not None and i2 is not None:
                if i2[0] - i1[1] >= MINIMUM_GAP_BETWEEN_POWER_CUTS:
                    yield in1, in2


def convert_to_x(num, alphabet=BASE_ALPHABET):
    if (num == 0):
        return alphabet[0]
    arr = []
    base = len(alphabet)

    while num:
        rem = num % base
        num = num // base
        arr.append(alphabet[rem])
    arr.reverse()
    return "".join(arr)


def convert_from_x(string, alphabet=BASE_ALPHABET):
    base = len(alphabet)
    strlen = len(string)
    num = 0

    idx = 0
    for char in string:
        power = (strlen - (idx + 1))
        num += alphabet.index(char) * (base ** power)
        idx += 1

    return num


def get_element_day_interval(pos):
    gen = days_intervals()
    for x in range(0, pos - 1):
        gen.next()
    return gen.next()


'''
Okay, so the total number of possible interval-combos in a day
is baffeling.
It's obvious that I need some serious algorithms.
First off, lets note down the problem:
    * Make the number smaller
    * If we can't make the number smaller, atleast find a
    smaller way to represent the number
    * Find a fast method to search through the tree
Now, we have two points:
    * Denote the schedule for each day in the smallest manner possible
    &/or
    * Denote the total number(the combination of all 7 days of schedule)
    in the smallest way possible
'''


if __name__ == "__main__":
    s = (
            (8, 13, 19, 24),
            (7, 12, 16, 20.5),
            (4, 9, 11, 17),
            (5, 9.5, 12, 18),
            (6, 11, 14, 20),
            (9, 16, 20, 23),
            (9, 14, 18, 22.5),
        )

    print(sum([1 for _ in single_interval_generator()]))
    print(sum([1 for _ in days_intervals()]))
